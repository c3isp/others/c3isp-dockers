#!/bin/bash

credentials=./.credentials

source $credentials > /dev/null

if [ -z ${C3ISP_LDAP_USER} ]; then
    echo "Insert C3ISP LDAP credentials."
    echo -n "User: "
    read C3ISP_LDAP_USER
    echo -n "Password: "
    read -s C3ISP_LDAP_PASSWORD
    echo -e "C3ISP_LDAP_USER=$C3ISP_LDAP_USER\nC3ISP_LDAP_PASSWORD=$C3ISP_LDAP_PASSWORD"  > $credentials
    echo
fi

#!/bin/bash

source ./auth.sh

WGET_CMD="wget -q --auth-no-challenge --user $C3ISP_LDAP_USER --password $C3ISP_LDAP_PASSWORD"
soucelist=( isi iai portal )

for i in "${soucelist[@]}"
do
    while IFS= read -r line || [[ $line ]]
    do
        artifacts=(`$WGET_CMD -O- $line/api/xml?tree=artifacts[relativePath] | grep -oP '(?<=relativePath>)[^<]+'`)
        dest=volumes/$i/tomcat/webapps
        mkdir -p $dest >/dev/null
        for file in "${artifacts[@]}"
        do
            destfile=${dest}/${file#target\/}
            action=$([ -f ${destfile} ] && echo "🐳 updating   " || echo "🐋 downloading" )
            sudo rm -rf ${destfile%.*}
            sudo rm -rf ${destfile}
            [[ "$line" =~ ^#.*$ ]] && continue
            echo "$action";
            $WGET_CMD --show-progress $line/artifact/$file -O ${destfile};
            # mkdir ${destfile%.*}
            # unzip -q ${destfile} -d ${destfile%.*} && rm -f ${destfile}
        done
        [[ "$line" =~ ^#.*$ ]] && continue
        if [ ${#artifacts[@]} -eq 0 ]; then
            echo "🚨 No artifacts found for ${line}"
        fi
    done <  "warlists/${i}.warlist"
done

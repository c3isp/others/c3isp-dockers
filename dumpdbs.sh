#!/bin/bash

ISI_SSH="ssh isi"
ISI_MSQL_PSWD=mysqlc317
ISI_DIR=volumes/isi/db
mkdir -p $ISI_DIR >/dev/null
rm -f $ISI_DIR/*.sql >/dev/null

IAI_SSH="ssh iai"
IAI_MSQL_PSWD=serUCA-c3is17
IAI_DIR=volumes/iai/db
mkdir -p $IAI_DIR >/dev/null
rm -f $IAI_DIR/*.sql >/dev/null

isidblist=($(${ISI_SSH} "mysql --user=root -p${ISI_MSQL_PSWD} -e 'SHOW DATABASES;' 2>/dev/null | grep -Ev '(Database|information_schema|performance_schema|mysql|sys)'"))

for db in "${isidblist[@]}"
do
    echo "⏬ Downloading ISI DATABASE $db"
    ${ISI_SSH} "mysqldump --user=root -p${ISI_MSQL_PSWD} --databases ${db} 2>/dev/null | gzip -c" | gunzip  > "$ISI_DIR/$db.sql" 
done


iaidblist=($(${IAI_SSH} "mysql --user=root -p${IAI_MSQL_PSWD} -e 'SHOW DATABASES;' 2>/dev/null | grep -Ev '(Database|information_schema|performance_schema|mysql|sys)'"))

for db in "${iaidblist[@]}"
do
    echo "⏬ Downloading IAI DATABASE $db"
    ${IAI_SSH} "mysqldump --user=root -p${IAI_MSQL_PSWD} --databases ${db} 2>/dev/null | gzip -c" | gunzip  > "$IAI_DIR/$db.sql" 
done

cp $IAI_DIR/*.sql volumes/portal/db/

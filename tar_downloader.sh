#!/bin/bash

source ./auth.sh

WGET_CMD="wget -q --auth-no-challenge --user $C3ISP_LDAP_USER --password $C3ISP_LDAP_PASSWORD"
soucelist=( isi iai portal )


# download isi-node app
line=https://devc3isp.iit.cnr.it/jenkins/job/ISI_Information-Sharing-Infrastructure/job/format-adapter/lastSuccessfulBuild
file=format-adapter.tgz
dest=volumes/isi/node/apps
destfile=${dest}/${file}
action=$([ -f ${destfile} ] && echo "🐳 updating   " || echo "🐋 downloading" )
echo "$action"
mkdir -p $dest >/dev/null
$WGET_CMD --show-progress $line/artifact/$file -O $dest/$file;


# download iai-spam-detect app
line=https://devc3isp.iit.cnr.it/jenkins/job/IAI_CNR/job/spam-detect/lastSuccessfulBuild
file=spam-detect.tgz
dest=volumes/iai/dependencies/spam-detect
destfile=${dest}/${file}
action=$([ -f ${destfile} ] && echo "🐳 updating   " || echo "🐋 downloading" )
echo "$action"
mkdir -p $dest >/dev/null
$WGET_CMD --show-progress $line/artifact/$file -O $destfile;


# download iai-dga-detector app
line=https://devc3isp.iit.cnr.it/jenkins/job/IAI_CNR/job/dga-detector/lastSuccessfulBuild
file=dga-detector.tgz
dest=volumes/iai/dependencies/dga-detector
destfile=${dest}/${file}
action=$([ -f ${destfile} ] && echo "🐳 updating   " || echo "🐋 downloading" )
echo "$action"
mkdir -p $dest >/dev/null
$WGET_CMD --show-progress $line/artifact/$file -O $destfile;


# download iai-dga-collector app
line=https://devc3isp.iit.cnr.it/jenkins/job/IAI_CNR/job/dga-collector/lastSuccessfulBuild
file=c3isp_dga_collector-0.0.1-SNAPSHOT-jar-with-dependencies.jar
dest=volumes/iai/dependencies/dga-collector
destfile=${dest}/${file}
action=$([ -f ${destfile} ] && echo "🐳 updating   " || echo "🐋 downloading" )
echo "$action"
mkdir -p $dest >/dev/null
$WGET_CMD --show-progress $line/artifact/target/$file -O $destfile;
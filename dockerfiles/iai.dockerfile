FROM tomcat:8.5-jdk8-openjdk

RUN  apt update && apt install -y python3 python3-dev python3-pip

RUN mkdir -p /opt/isi/datalakebuffer \
/ConfigServer \
/opt/iai/dga_detector-master \
/opt/iai/dga_collector

COPY ./commands/iai.sh .
COPY ./commands/common/wait-for-it.sh .

EXPOSE 8443 8080 443 80

CMD ["./iai.sh"]
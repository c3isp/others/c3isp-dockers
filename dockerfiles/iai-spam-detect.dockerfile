FROM python:3.6

RUN mkdir -p /app/spam-detect/SpamDetector/ClassifierFiles

RUN cd /app/spam-detect/SpamDetector/ClassifierFiles && \
wget https://dl.fbaipublicfiles.com/fasttext/vectors-english/crawl-300d-2M.vec.zip && \
unzip crawl-300d-2M.vec.zip && \
rm crawl-300d-2M.vec.zip 

COPY ./commands/iai-spam-detect.sh /

EXPOSE 9999

CMD ["./iai-spam-detect.sh"]

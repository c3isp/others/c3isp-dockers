FROM node:8.16.1

COPY ./commands/isi-node.sh .
COPY ./commands/common/wait-for-it.sh .

CMD ["./isi-node.sh"]
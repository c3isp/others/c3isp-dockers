FROM tomcat:8.5-jdk8-openjdk

RUN mkdir -p /opt/isi/datalakebuffer \
/opt/localdpos/data \
/ConfigServer

COPY ./commands/isi.sh .
COPY ./commands/common/wait-for-it.sh .

EXPOSE 8443 8080 443 80

CMD ["./isi.sh"]
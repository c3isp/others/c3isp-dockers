#!/bin/bash

mkdir format-adapter
cd /opt
tar zxvf format-adapter.tgz -C ../format-adapter
cd ../format-adapter
npm cache clean --force 
npm install
cd ..
./wait-for-it.sh localhost:8888 --timeout=${WAIT_TIMEOUT} -- node format-adapter/converter.js

#!/bin/bash

# Extract dga detector python script and install dependencies
tar zxvf /opt/iai/src/dga-detector/dga-detector.tgz -C /opt/iai/dga_detector-master
pip3 install -r /opt/iai/dga_detector-master/requirements.txt

# Copy dga collector java cron and launch it
cp /opt/iai/src/dga-collector/c3isp_dga_collector-0.0.1-SNAPSHOT-jar-with-dependencies.jar /opt/iai/dga_collector/c3isp_dga_collector-0.0.1-SNAPSHOT.jar
./wait-for-it.sh localhost:3306 --timeout=${WAIT_TIMEOUT} -- java -jar /opt/iai/dga_collector/c3isp_dga_collector-0.0.1-SNAPSHOT.jar &

# Run ConfigServer Spring app to serve the configuration to all isi projects
/ConfigServer/ConfigServer-0.0.1-SNAPSHOT.jar &

# Whait for config server and mysql before launching Tomcat 
./wait-for-it.sh localhost:8888 --timeout=${WAIT_TIMEOUT} -- \
./wait-for-it.sh localhost:3306 --timeout=${WAIT_TIMEOUT} -- ./bin/catalina.sh run
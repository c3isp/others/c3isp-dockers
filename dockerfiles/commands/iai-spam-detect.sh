#!/bin/bash

tar zxvf /master-tar/spam-detect/spam-detect.tgz -C /app/spam-detect

pip3 install -r /app/spam-detect/requirements.txt

cd /app/spam-detect

python3 -u app.py
#!/bin/bash

# Run ConfigServer Spring app to serve the configuration to all isi projects
/ConfigServer/ConfigServer-0.0.1-SNAPSHOT.jar &

# Whait for config server and mysql before launching Tomcat
./wait-for-it.sh localhost:8888 --timeout=${WAIT_TIMEOUT} -- \
./wait-for-it.sh localhost:3306 --timeout=${WAIT_TIMEOUT} -- ./bin/catalina.sh run
FROM tomcat:8.5-jdk8-openjdk

RUN keytool -genkey -noprompt \
 -alias portal \
 -dname "CN=portal.iit.cnr.it, OU=IIT, O=CNR, L=Unknown, S=Unknown, C=IT" \
 -keystore conf/keystore.jks \
 -storepass changeit \
 -keypass changeit \
 -keyalg RSA
 
EXPOSE 8443 8080 443 80

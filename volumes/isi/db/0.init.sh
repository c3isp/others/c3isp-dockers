#!/bin/sh

mysql -u root -p"${MYSQL_ROOT_PASSWORD}" << END
    CREATE USER 'bmuser'@'127.0.0.1' IDENTIFIED BY 'AAff33+11-aad55X00';
    GRANT ALL PRIVILEGES ON *.* TO 'bmuser'@'127.0.0.1';
    flush privileges;
END
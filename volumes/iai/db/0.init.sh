#!/bin/sh

mysql -u root -p"${MYSQL_ROOT_PASSWORD}" << END
    CREATE USER 'serviceuca'@'127.0.0.1' IDENTIFIED WITH mysql_native_password BY 'serUCA-c3is17';
    GRANT ALL PRIVILEGES ON *.* TO 'serviceuca'@'127.0.0.1';
    CREATE USER 'dgauser'@'127.0.0.1' IDENTIFIED WITH mysql_native_password BY 'AXfr4+h+11-0022uy';
    GRANT ALL PRIVILEGES ON *.* TO 'dgauser'@'127.0.0.1';
    flush privileges;
END

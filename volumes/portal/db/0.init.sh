#!/bin/sh

mysql -u root -p"${MYSQL_ROOT_PASSWORD}" << END
    CREATE USER 'serviceuca'@'127.0.0.1' IDENTIFIED BY 'serUCA-c3is17';
    GRANT ALL PRIVILEGES ON *.* TO 'serviceuca'@'127.0.0.1';
    flush privileges;
END
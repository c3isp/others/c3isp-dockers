#!/bin/bash

# Used to deploy one or more specific ISI component when something goes wrong.
# This script just redeploy the components by removing and adding them again from tomcat
#
# @params
# $1: the system name "isi" or "iai" or "portal"
# $2... : the component name without .war extension
#
# @output: none
#
# Usage: redeployIsiComponents.sh [list_of_components_space_separed]
# i.e. redeployIsiComponents.sh multi-resource-handler dsa-adapter-frontend event-handler

component=

for warname in "$@"
do
  if [ "$warname" == "isi" ]; then
    component=isi
    continue
  fi
  if [ "$warname" == "iai" ]; then
    component=iai
    continue
  fi
  if [ "$warname" == "portal" ]; then
    component=portal
    continue
  fi

  webappspath=../volumes/$component/tomcat/webapps
  echo "redeploying $warname..."

  warfolder="$webappspath/$warname"
  warpath="$warfolder.war"

  if [ ! -e $warpath ]; then
    echo "$warpath does not exists!"
  else
    mv $warpath .
    sudo rm -r $warfolder
    mv $warname.war $webappspath
    echo "done"
  fi
done

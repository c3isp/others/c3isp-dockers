#!/bin/bash

# Use this script to download all the .war files from a given machine
# In order to use this script you MUST have a valid configuration on your
# ~/.ssh/config file, with your publickey deployed on the system you want connect with
#
# @params:
#   $1: the hostname of the machine you want to connect with
# @output: all the .war files already deployed into the correct tomcat 

machine="$1"
location=
isiFolder=../volumes/isi/tomcat/webapps
isibkp=../volumes/isi/tomcat/warbkp
iaiFolder=../volumes/iai/tomcat/webapps
iaibkp=../volumes/iai/tomcat/warbkp
portalFolder=../volumes/portal/tomcat/webapps
portalbkp=../volumes/portal/tomcat/warbkp

echo "Removing old ISI backups..."
if [ -e $isibkp ]; then
  rm -r $isibkp
fi
mkdir $isibkp

echo "doing a backup of ISI WARs..."
cp -r $isiFolder $isibkp
echo "Done"

echo "Removing old IAI backups..."
if [ -e $iaibkp ]; then
  rm -r $iaibkp
fi
mkdir $iaibkp

echo "doing a backup of IAI WARs..."
cp -r $iaiFolder $iaibkp
echo "Done"

echo "Removing old Portal backups..."
if [ -e $portalbkp ]; then
  rm -r $portalbkp
fi
mkdir $portalbkp

echo "doing a backup of Portal WARs..."
cp -r $portalFolder $portalbkp
echo "Done"

echo "copying WARs from $machine to ISI"
if [[ $machine == "sane" ]]; then
  location=/home/cybersane/c3isp-dockers/volumes/isi/tomcat/webapps/*.war
fi
scp -r $machine:$location $isiFolder
echo "Done"

echo "Copying WARs from $machine to IAI"
if [[ $machine == "sane" ]]; then
  location=/home/cybersane/c3isp-dockers/volumes/iai/tomcat/webapps/*.war
fi
scp -r $machine:$location $iaiFolder
echo "Done"

echo "Copying WARs from $machine to Portal"
if [[ $machine == "sane" ]]; then
  location=/home/cybersane/c3isp-dockers/volumes/portal/tomcat/webapps/*.war
fi
scp -r $machine:$location $portalFolder
echo "Done"


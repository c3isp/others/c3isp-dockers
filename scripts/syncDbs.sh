#!/bin/bash

# Use this script to download dumped databases from another c3isp machine.
# The databases will be then imported into this machine dockers
#
# Please note that to use this script you need a valid configuration
# on ~/.ssh/config file, with your publickey deployed on the system you want connect with
# in order to copy databases from that machine
#
# IMPORTANT: this operation will overwrite the old databases in this machine containers!
#
# @params
# $1: the hostname of the machine you want to copy from. It MUST be the same of the one contained
#     into the ~/.ssh/config file
# @output: a "dumpedDB" folder containing "isi.sql" and "iai.sql" files (already imported into
#          the containers

hostname="$1"

hostScriptsFolder=/home/$hostname/c3isp-dockers/scripts
testScriptsFolder=~/calo/c3isp-dockers/scripts
dbFolder=$testScriptsFolder/dumpedDB

echo "Remove tmp stuff..."
if [ -e $dbFolder ]; then
  rm -r $dbFolder
fi
mkdir $dbFolder

echo -n "Insert $hostname sudo password: "
read -s hostpw

echo -e "\nDumping databases from $hostname docker"
ssh -t $hostname >/dev/null 2>&1 <<ENDSSH
cd $hostScriptFolder
echo '$hostpw' | sudo -S ./dumpDB.sh
ENDSSH

echo "Copying databases into dumpedDB folder"
scp -r $hostname:$hostScriptsFolder/dumpedDB .
#scp -r testc3isp:~/calo/c3isp-dockers/scripts/dumpedDB .

echo "Importing databases into containers"
cd ..

sudo docker cp $dbFolder/isi.sql c3isp-dockers_isi-db_1:/
sudo docker cp $dbFolder/iai.sql c3isp-dockers_iai-db_1:/
sudo docker cp $dbFolder/iai.sql c3isp-dockers_portal-db_1:/

sudo docker exec -it c3isp-dockers_isi-db_1 sh -c 'mysql --user root --password=mysqlc317 < isi.sql'
sudo docker exec -it c3isp-dockers_iai-db_1 sh -c 'mysql --user root --password=serUCA-c3is17 < iai.sql'
sudo docker exec -it c3isp-dockers_portal-db_1 sh -c 'mysql --user root --password=serUCA-c3is17 < iai.sql'

echo "Removing tmp files from $hostname"
ssh -t $hostname >/dev/null 2>&1 <<ENDSSH
cd $hostScriptsFolder
echo '$hostpw' | sudo -S rm -r dumpedDB
ENDSSH

echo "Done!"

#!/bin/bash

# used to open the logs of all containers
# @params: none
# @output: logs of all containers

clear
sudo docker-compose logs -f --tail=1

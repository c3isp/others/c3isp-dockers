#!/bin/bash

# This script is used to delete all DPOs with the given event type from ISI container
#
# @param $1: eventType
# @output: the list of DPOs deleted 

sudo docker exec -it c3isp-dockers_isi_1 /root/deleteByEventType.sh $1

#!/bin/bash

# Used to renew isi, iai and nodeJS certificates. To run this script you need:
#   - isi sudo password
#   - iai sudo password
#   - ssh connection with keys for isi and iai setted up and valid configuration in your ~/.ssh/config file
# Please note that at the very end of this script, ALL the docker components will be restarted!
#
# @params: none
# @output: two keystore.jks file (one for ISI, one for IAI), one fullchain.pem and one privkey.pem files (for nodejs)

echo "connecting to ISI to prepare files to copy..."
echo -n "Insert the ISI sudo password: "
read -s isipw

ssh -t isi >/dev/null 2>&1 <<ENDSSH
if [ -e tempCertCopyIsi ]; then
  rm -r tempCertCopyIsi
fi
mkdir tempCertCopyIsi
echo '$isipw' | sudo -S cp /etc/letsencrypt/live/isic3isp.iit.cnr.it/keystore.jks tempCertCopyIsi
echo '$isipw' | sudo -S cp /etc/letsencrypt/live/isic3isp.iit.cnr.it/privkey.pem tempCertCopyIsi
echo '$isipw' | sudo -S cp /etc/letsencrypt/live/isic3isp.iit.cnr.it/fullchain.pem tempCertCopyIsi
echo '$isipw' | sudo -S chown -R isic3isp:isic3isp tempCertCopyIsi
ENDSSH

echo "copying files from ISI..."
scp -r isi:~/tempCertCopyIsi .

echo -e "\nconnecting to IAI to prepare files to copy"
echo -n "Insert the IAI sudo password: "
read -s iaipw

ssh -t iai >/dev/null 2>&1 <<ENDSSH
if [ -e tempCertCopyIai ]; then
  rm -r tempCertCopyIai
fi
mkdir tempCertCopyIai
echo '$iaipw' | sudo -S cp /etc/letsencrypt/live/iaic3isp.iit.cnr.it/keystore.jks tempCertCopyIai
echo '$iaipw' | sudo -S chown -R iaic3isp:iaic3isp tempCertCopyIai
ENDSSH

echo "copying files from IAI..."
scp -r iai:~/tempCertCopyIai .

isitomcat=../volumes/isi/tomcat/conf
iaitomcat=../volumes/iai/tomcat/conf
isinode=../volumes/isi/node/keys

echo -e "\nrenewing isi certificate"
mv $isitomcat/keystore.jks $isitomcat/keystore.jks.bak
mv tempCertCopyIsi/keystore.jks $isitomcat

echo "renewing isi-node certificate"
mv $isinode/fullchain.pem $isinode/fullchain.pem.bak
mv $isinode/privkey.pem $isinode/privkey.pem.bak
mv tempCertCopyIsi/fullchain.pem $isinode
mv tempCertCopyIsi/privkey.pem $isinode

echo "renewing iai certificate"
mv $iaitomcat/keystore.jks $iaitomcat/keystore.jks.bak
mv tempCertCopyIai/keystore.jks $iaitomcat

echo -e "\nremoving tmp stuff..."
rm -r tempCertCopyIai
rm -r tempCertCopyIsi

echo -e "\nrestarting docker-compose..."
sudo docker-compose stop && sudo docker-compose up -d
echo -e "\nDone!"


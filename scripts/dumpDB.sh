#!/bin/bash

#Use this script to dump ISI and IAI databases from the corrispectives containers
# @params: none
# @output: "dbFolder" containing "isi.sql" and "iai.sql" files

dbFolder=scripts/dumpedDB

cd ..

isiDBContainer=c3isp-dockers_isi-db_1
iaiDBContainer=c3isp-dockers_iai-db_1

if [ -e $dbFolder ]; then
  rm -r $dbFolder
fi
mkdir $dbFolder

echo "Dumping ISI databases..."
sudo docker exec -it c3isp-dockers_isi-db_1 sh -c 'mysqldump --user root --password=mysqlc317 --all-databases > isi.sql'
echo "Dumping IAI databases..."
sudo docker exec -it c3isp-dockers_iai-db_1 sh -c 'mysqldump --user root --password=serUCA-c3is17 --all-databases > iai.sql'

sudo docker cp $isiDBContainer:/isi.sql $dbFolder
sudo docker cp $iaiDBContainer:/iai.sql $dbFolder

echo "Done!"


